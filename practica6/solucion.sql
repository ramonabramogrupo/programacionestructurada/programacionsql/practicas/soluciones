﻿/**
  Practica 6 
  Procedimientos y funciones 
**/

DROP DATABASE IF EXISTS practica6programacion;
CREATE DATABASE practica6programacion;
USE practica6programacion;


/**
  Ejercicio 1
  Crea un procedimiento simple que muestre por pantalla el texto "esto
  es un ejemplo de procedimiento" (donde la cabecera de la columna sea “mensaje”).
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE ejemplo1()
  COMMENT 'procedimiento que saca mensaje por pantalla'
BEGIN
/* variables */
/* cursores */
/* control excepciones */
/* programa */
  SELECT "esto es un ejemplo de procedimiento" AS mensaje;
END //
DELIMITER ;


-- llamamos al procedimiento

  CALL ejemplo1();

/**
  Ejercicio 2
  Crea un procedimiento donde se declaren tres variables internas, una de tipo entero con valor de 1, 
  otra de tipo varchar(10) con valor nulo por defecto y otra de tipo decimal con 4 dígitos y 
  dos decimales con valor 10,48 por defecto. Dentro del procedimiento, modificar 
  el valor de la variable entera,   
  la variable de tipo texto, realizar alguna operación matemáticas con las variables y 
  mostrar los resultados en una select.
**/
DELIMITER //
CREATE OR REPLACE PROCEDURE ejemplo2()
  COMMENT 'Procedimiento con variables'
BEGIN
  /* variables */
  DECLARE numero int DEFAULT 1;
  DECLARE texto varchar(10) DEFAULT NULL;
  DECLARE numeroReal float(4,2) DEFAULT 10.48;

/* cursores */
/* control excepciones */
/* programa */
  SET numero=10;
  SET texto="Ejemplo de clase";
  SELECT numero+numeroReal;

END //
DELIMITER ;

-- llamar al procedimiento
  CALL ejemplo2();

/**
  Ejercicio 3

  Corregir los errores para entender los enum y los varchar
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE ejemplo3()
  COMMENT 'funcionamiento de los enum y varchar' 
BEGIN
/* variables */
  DECLARE v_caracter1 char(1);
  DECLARE forma_pago enum('metálico', 'tarjeta', 'transferencia');
  /* cursores */
  /* control excepciones */
  /* programa */
  SET v_caracter1 = 'hola'; -- solamente se queda con la h
  SELECT v_caracter1;
  SET forma_pago = 1; -- se queda con metalico
  SELECT forma_pago;  
  SET forma_pago = 'cheque';  -- no es valido y lo deja null
  SELECT forma_pago;  
  SET forma_pago = 4; -- no es valido y lo deja null
  SELECT forma_pago;  
  SET forma_pago = 'TARJETA';
  SELECT forma_pago;  
 
END //
DELIMITER ;

-- llamar
  CALL ejemplo3();

  /**
  Ejercicio 4
  Escribe un procedimiento que reciba un parámetro de entrada (numero real) 
  y muestre el numero en pantalla.
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE ejemplo4 (IN numero float)
  COMMENT 'Procedimiento para utilizar argumentos de entrada'
BEGIN
/* variables */
/* cursores */
/* excepciones */
/* programa */
  SELECT numero;
END //
DELIMITER ;

CALL ejemplo4(23);

/**
  Ejercicio 5
  Escribe un procedimiento que reciba un parámetro de entrada y asigne ese parámetro
  a una variable interna del mismo tipo. Despues mostrar la variable en pantalla.
**/

DELIMITER //
CREATE OR REPLACE PROCEDURE ejemplo5 (IN aNumero float)
  COMMENT 'Procedimiento para utilizar argumentos de entrada y variables'
BEGIN
/* variables */
  DECLARE vNumero float;
/* cursores */
/* excepciones */
/* programa */
  SET vNumero=aNumero;

  SELECT vNumero;
END //
DELIMITER ;

CALL ejemplo5(23);

/* Ejercicio 6
   Escribe una función que devuelva el valor de la hipotenusa de un triángulo a partir de los valores de sus lados.
  */

  DELIMITER //
    DROP FUNCTION IF EXISTS ejemplo6//
    CREATE FUNCTION ejemplo6(lado1 int, lado2 int)
      RETURNS float
      COMMENT 'calcular la hipotenusa de un triangulo'
      BEGIN
      /** variables **/
        DECLARE resultado float;
      /** programa **/
        SET resultado=SQRT(POW(lado1,2)+POW(lado2,2));
      /** retorna **/
      RETURN resultado;
      END //
    DELIMITER ;

SELECT ejemplo6(2,2);


/**
  Ejercicio 7
  Realizar el ejercicio anterior utilizando procedimientos. 
  Debeis crear un procedimiento con 3 parámetros (dos parámetros de entrada con los lados del 
  triangulo y un parámetro de salida que es la hipotenusa calculada)
 **/

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejemplo7 //
  CREATE PROCEDURE ejemplo7(lado1 int,lado2 int, OUT resultado float)
      COMMENT 'calcular la hipotenusa de un triangulo'
      BEGIN
      /** variables **/
      /** programa **/
        SET resultado=SQRT(POW(lado1,2)+POW(lado2,2));
      /** retorna **/
      END //
  DELIMITER ;


-- llamar al procedimiento

  call ejemplo7(2,3,@r);
  SELECT @r;