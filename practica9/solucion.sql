﻿USE facturas;

/** procedimiento 1 **/

DROP PROCEDURE IF EXISTS actualizarProductos;
CREATE PROCEDURE actualizarProductos()
BEGIN
-- declarar variables
UPDATE productos p JOIN grupos g ON p.grupo = g.id
  set 
    p.nombreGrupo=g.nombre, 
    p.ivaGrupo=g.iva;

-- procesamiento

END;

/** procedimiento 2 **/

DROP PROCEDURE IF EXISTS actualizarProducto;
CREATE PROCEDURE actualizarProducto(id int)
BEGIN
-- declarar variables
UPDATE productos p JOIN grupos g ON p.grupo = g.id
  SET p.nombreGrupo=g.nombre, p.ivaGrupo=g.iva
  WHERE p.id=id;

-- procesamiento

END;

/**
procedimiento 3
**/


DROP PROCEDURE IF EXISTS actualizarLineas;
CREATE PROCEDURE actualizarLineas()
BEGIN
-- declarar variables

-- procesamiento
UPDATE lineas l JOIN productos p ON l.producto = p.id
  SET 
    l.nombreProducto=p.nombre, 
    l.precio=p.precio, 
    l.total=l.precio*l.cantidad;

END;


/**
procedimiento 4
**/
DROP PROCEDURE IF EXISTS actualizarLinea;
CREATE PROCEDURE actualizarLinea(id int)
BEGIN
-- declarar variables

-- procesamiento
UPDATE lineas l JOIN productos p ON l.producto = p.id
  SET l.nombreProducto=p.nombre, l.precio=p.precio, l.total=l.precio*l.cantidad
  WHERE l.id=id;

END;


CREATE or REPLACE VIEW lineasIVA AS 
  SELECT p.ivaGrupo*l.total ivaCalculado,l.id,l.total,l.factura FROM productos p JOIN lineas l ON p.id = l.producto; 


/**
procedimiento 5
**/

DROP PROCEDURE IF EXISTS actualizarFacturas;
CREATE PROCEDURE actualizarFacturas()
BEGIN

UPDATE factura f JOIN 
  (SELECT l.factura,SUM(l.total*p.ivaGrupo) ivaCalculado,SUM(l.total) totalCalculado FROM lineas l JOIN productos p ON l.producto = p.id GROUP BY l.factura) c1  
  ON c1.factura=f.id
  set 
    f.iva=c1.ivaCalculado,
    f.total=c1.totalCalculado,
    f.totalIva=f.total+f.iva;
  

-- procesamiento

/*
utilizando vista
UPDATE factura f JOIN (
  SELECT l.factura,SUM(l.total) suma,SUM(l.ivaCalculado) sumaIVA FROM lineasIVA l GROUP BY l.factura) c1
  ON c1.factura=f.id
set total=c1.suma, f.iva=c1.sumaIVA, f.totalIva=f.total+f.iva;*/

END;

DROP PROCEDURE IF EXISTS actualizarFactura;
CREATE PROCEDURE actualizarFactura(id int)
BEGIN

UPDATE factura f JOIN 
  (SELECT l.factura,SUM(l.total*p.ivaGrupo) ivaCalculado,SUM(l.total) totalCalculado FROM lineas l JOIN productos p ON l.producto = p.id GROUP BY l.factura) c1  
  ON c1.factura=f.id
  set 
    f.iva=c1.ivaCalculado,
    f.total=c1.totalCalculado,
    f.totalIva=f.total+f.iva
  WHERE f.id=id;

/**
utilizando vista 
  UPDATE factura f JOIN (
  SELECT l.factura,SUM(l.total) suma,SUM(l.ivaCalculado) sumaIVA FROM lineasIVA l GROUP BY l.factura) c1
  ON c1.factura=f.id
set total=c1.suma, f.iva=c1.sumaIVA, f.totalIva=f.total+f.iva
WHERE f.id=id;
**/

END;



/**
disparador 1
**/

-- opcion 1
DROP TRIGGER IF EXISTS productosBI;
CREATE TRIGGER productosBI
BEFORE INSERT
  ON productos
  FOR EACH ROW
BEGIN
  -- variables
  DECLARE vNombre varchar(100) DEFAULT NULL;
  DECLARE vIva float DEFAULT 0;
  
  -- leo los valores
  SELECT 
      nombre,iva INTO vNombre,vIva 
    FROM grupos WHERE id=NEW.grupo;

  -- los almaceno en los campos
  SET NEW.nombreGrupo=vNombre,NEW.ivaGrupo=vIva;

END ;

-- opcion 2

DROP TRIGGER IF EXISTS productosBI;
CREATE TRIGGER productosBI
BEFORE INSERT
  ON productos
  FOR EACH ROW
BEGIN
  SET 
    NEW.nombreGrupo=(SELECT nombre FROM grupos WHERE id=NEW.grupo),
    NEW.ivaGrupo=(SELECT iva FROM grupos WHERE id=NEW.grupo);

END ;

/**
disparador 2
**/

DROP TRIGGER IF EXISTS productosBU;
CREATE TRIGGER productosBU
BEFORE UPDATE
  ON productos
  FOR EACH ROW
BEGIN
   SET 
    NEW.nombreGrupo=(SELECT nombre FROM grupos WHERE id=NEW.grupo),
    NEW.ivaGrupo=(SELECT iva FROM grupos WHERE id=NEW.grupo);
END ;

/**
disparador 3
**/

-- opcion 1
DROP TRIGGER IF EXISTS lineasBI;
CREATE TRIGGER lineasBI
BEFORE INSERT
  ON lineas
  FOR EACH ROW
BEGIN
  -- variables
  DECLARE vNombre varchar(100) DEFAULT NULL;
  DECLARE vPrecio float DEFAULT 0;
  DECLARE vTotal float DEFAULT 0;

  -- leo los valores
  SELECT 
      p.nombre,p.precio INTO vNombre,vPrecio 
    FROM productos p 
    WHERE id=NEw.producto;

  -- asigno los valores a los campos
  SET 
    NEW.nombreProducto=vNombre,
    NEW.precio=vPrecio,
    NEW.total=NEW.cantidad*vPrecio;
  
END ;

-- opcion 2
DROP TRIGGER IF EXISTS lineasBI;
CREATE TRIGGER lineasBI
BEFORE INSERT
  ON lineas
  FOR EACH ROW
BEGIN
  SET NEW.nombreProducto=(SELECT p.nombre FROM productos p WHERE id=NEW.producto);
  SET NEW.precio=(SELECT p.precio FROM productos p WHERE id=NEW.producto);
  SET NEW.total=NEW.cantidad*NEW.precio;
END ;



/** 
disparador 4
**/

DROP TRIGGER IF EXISTS lineasBU;
CREATE TRIGGER lineasBU
BEFORE UPDATE
  ON lineas
  FOR EACH ROW
BEGIN
  SET NEW.nombreProducto=(SELECT p.nombre FROM productos p WHERE id=NEW.producto);
  SET NEW.precio=(SELECT p.precio FROM productos p WHERE id=NEW.producto);
  SET NEW.total=NEW.cantidad*NEW.precio;
END ;

/** 
disparador 5
**/

DROP TRIGGER IF EXISTS lineasAI;
CREATE TRIGGER lineasAI
AFTER INSERT
  ON lineas
  FOR EACH ROW
BEGIN
  CALL actualizarFactura(NEW.factura);
END ;


/**
disparador 6
**/
DROP TRIGGER IF EXISTS lineasAU;
CREATE TRIGGER lineasAU
AFTER UPDATE
  ON lineas
  FOR EACH ROW
BEGIN
  CALL actualizarFactura(NEW.factura);

  IF (NEW.factura<>OLD.factura) THEN
    CALL actualizarFactura(OLD.factura);
  END IF;
  
END ;

/**
disparador 7
**/

DROP TRIGGER IF EXISTS lineasAD;
CREATE TRIGGER lineasAD
AFTER DELETE
  ON lineas
  FOR EACH ROW
BEGIN
  CALL actualizarFactura(OLD.factura);
END ;



