
-- EJERCICIO 2
   -- crear la vista
   CREATE OR REPLACE VIEW ejercicio2 AS 
   SELECT 
      COUNT(*)cuenta_ciudades
    FROM
      CIUDAD;
  -- llamar la vista
    SELECT * FROM ejercicio2 e;

-- EJERCICIO 3
  -- Crear la vista
  CREATE OR REPLACE VIEW ejercicio3 AS 
  SELECT
      nombre 
  FROM 
      ciudad
  WHERE
      poblaci�n > (SELECT AVG(poblaci�n) FROM ciudad);

  -- Llamar a la vista
  SELECT * FROM ejercicio3 e;


-- EJERCICIO 4
  CREATE OR REPLACE VIEW ejercicio4 AS 
  SELECT 
      nombre 
  FROM 
    ciudad 
  WHERE
    poblaci�n <(SELECT AVG(poblaci�n) FROM ciudad);

  SELECT * FROM ejercicio4 e;

-- EJERCICIO 5
  CREATE OR REPLACE VIEW ejercicio5 AS 
  SELECT 
    nombre
  FROM
    ciudad
  WHERE
    poblaci�n=(SELECT MAX(poblaci�n) FROM ciudad);

  SELECT * FROM ejercicio5 e;


-- EJERCICIO 6
  CREATE OR REPLACE VIEW ejercicio6 AS 
  SELECT 
      COUNT(*)
  FROM
    ciudad 
   WHERE 
      poblaci�n>(SELECT AVG(poblaci�n) FROM ciudad c);
  
  SELECT * FROM ejercicio6;

-- EJERCICIO 7
  CREATE OR REPLACE VIEW ejercicio7 AS 
  SELECT
      ciudad, COUNT(*)    
  FROM
      persona JOIN ciudad c ON c.nombre=ciudad
  GROUP BY ciudad ;

  SELECT * FROM ejercicio7 e;



-- EJERCICIO 8
  CREATE OR REPLACE VIEW ejercicio8 AS 
  SELECT t.compa�ia, COUNT(*)
   FROM
      trabaja t
    GROUP BY compa�ia;

  SELECT * FROM ejercicio8 e;


-- EJERCICIO 9 

-- SUBCONSULTA 1
CREATE OR REPLACE VIEW ejercicio9c1 AS 
SELECT t.compa�ia, COUNT(*) AS cuenta FROM trabaja t GROUP BY t.compa�ia;

CREATE OR REPLACE VIEW ejercicio9 AS 
  SELECT 
   c1.compa�ia
   FROM 
    ejercicio9c1 c1
  WHERE 
    c1.cuenta = (SELECT MAX(c1.cuenta) FROM ejercicio9c1 c1);

SELECT * FROM ejercicio9 e;


-- 9 con having
  CREATE OR REPLACE VIEW ejercicio9Having AS 
  SELECT 
    t.compa�ia
  FROM 
    trabaja t
  GROUP BY t.compa�ia
  HAVING COUNT(*) = (SELECT MAX(c1.cuenta) FROM ejercicio9c1 c1);
   


-- EJERCICIO 10
  CREATE OR REPLACE VIEW ejercicio10 AS 
  SELECT p.nombre, p.calle, c.poblaci�n 
    FROM 
    ciudad c 
    JOIN persona p 
    ON c.nombre=p.ciudad; 
  SELECT * FROM ejercicio10 e;

-- EJERCICIO11 
  
  CREATE OR REPLACE VIEW ejercicio11 AS 
  SELECT 
      c2.nombre, 
      c2.ciudad, 
      c.ciudad ciudad_compa�ia 
  FROM 
    compa�ia c 
    JOIN
         (SELECT
             c1.nombre,
             c1.ciudad,
             t.compa�ia 
           FROM 
             trabaja t 
                 JOIN 
                    (SELECT p.nombre,ciudad FROM persona p) AS c1 
                 ON t.persona=c1.nombre) AS c2 
     ON c2.compa�ia=c.nombre;

  SELECT * FROM ejercicio11 e;

-- 14 
SELECT p.nombre
  FROM persona p,trabaja t, compa�ia c
  WHERE p.nombre=t.persona
  AND t.compa�ia=c.nombre
  AND c.ciudad =p.ciudad
  ORDER BY p.nombre;


-- 15
SELECT 
  s.persona,
  s.supervisor 
FROM supervisa s;

-- 16 Forma 1
  
SELECT * FROM supervisa s;
SELECT * FROM persona p;

SELECT 
  c1.supervisor,
  c1.persona,
  c1.ciudadsup,
  persona.ciudad
FROM 
  (SELECT 
    s.supervisor, s.persona, ciudad AS ciudadsup 
    FROM
      supervisa s JOIN persona p 
    ON s.supervisor=p.nombre
    ) c1 JOIN persona 
      on persona=nombre;
-- 16 Forma 2


SELECT c2.supervisor,c2.persona,c2.ciudad ciudadsup, p.ciudad FROM (
  SELECT
    s.supervisor,
    persona,
    ciudad
 FROM 
    supervisa s JOIN persona p 
  ON s.supervisor=p.nombre) c2 JOIN persona p 
  ON c2.persona=nombre;

-- 17
SELECT COUNT(*) FROM (SELECT DISTINCT c.ciudad FROM compa�ia c) AS c1;
  
-- 18 
SELECT COUNT(*) FROM (SELECT DISTINCT p.ciudad  FROM persona p) AS c1;
-- 19
SELECT 
    t.persona 
  FROM 
      trabaja t
  WHERE 
      t.compa�ia='fagor';

-- 20 
  SELECT 
    t.persona 
  FROM 
    trabaja t
  WHERE 
    t.compa�ia<>'fagor';

-- 21
SELECT 
    t.persona 
  FROM 
    trabaja t
  WHERE 
    t.compa�ia='indra';

  -- 22
SELECT 
  t.persona 
FROM 
  trabaja t
 WHERE 
    t.compa�ia='fagor'
    OR
    t.compa�ia='indra'; 
-- 23
  SELECT
    c1.poblaci�n, 
    t.salario, 
    c1.nombre,
    t.compa�ia
FROM 
  trabaja t 
    JOIN  (
            SELECT 
              c.poblaci�n,
              p.nombre 
            FROM  ciudad c 
              JOIN persona p 
              ON c.nombre=ciudad) AS c1 
    ON nombre=persona;