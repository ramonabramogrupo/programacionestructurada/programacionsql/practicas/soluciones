﻿
USE compañias;


ALTER TABLE trabaja 
  ADD CONSTRAINT FK_trabaja_compañia_nombre FOREIGN KEY (compañia)
    REFERENCES compañia(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE trabaja 
  ADD CONSTRAINT FK_trabaja_persona_nombre FOREIGN KEY (persona)
    REFERENCES persona(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;



ALTER TABLE compañia 
  ADD CONSTRAINT FK_compañia_ciudad_nombre FOREIGN KEY (ciudad)
    REFERENCES ciudad(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE persona 
  ADD CONSTRAINT FK_persona_ciudad_nombre FOREIGN KEY (ciudad)
    REFERENCES ciudad(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE supervisa 
  ADD CONSTRAINT FK_supervisa_persona_nombre FOREIGN KEY (supervisor)
    REFERENCES persona(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE supervisa 
  ADD CONSTRAINT FK_supervisa_persona_nombre1 FOREIGN KEY (persona)
    REFERENCES persona(nombre) ON DELETE NO ACTION ON UPDATE NO ACTION;