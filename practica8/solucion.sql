﻿/**
  Ejemplos 1 de Programacion para MYSQL
**/


DROP DATABASE IF EXISTS EjemploProgramacion1;
CREATE DATABASE EjemploProgramacion1;
USE EjemploProgramacion1;

/*
  Ejercicio 1
  Realizar un procedimiento almacenado que reciba dos números y te indique el mayor de ellos 
  (realizarle con instrucción if, con consulta de totales y con una función de Mysql).
*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej1v1a//
CREATE PROCEDURE ej1v1a (n1 int, n2 int)
  COMMENT 'Realizar un procedimiento almacenado que reciba dos números y te indique el mayor de ellos, realizarle con instrucción if'
BEGIN
  -- definir variables e inicializarlas
  DECLARE s int DEFAULT 0;
  -- instruccion de seleccion y salidas
  IF (n1 > n2)
  THEN
    SELECT
      n1;
  ELSE
    SELECT
      n2;
  END IF;
END//
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS ej1v1b//
CREATE PROCEDURE ej1v1b (n1 int, n2 int)
  COMMENT 'Realizar un procedimiento almacenado que reciba dos números y te indique el mayor de ellos, realizarle con instrucción if
  mejoramos el algoritmo'
BEGIN
  -- definir variables e inicializarlas
  DECLARE s int DEFAULT 0;

  -- seleccion
  IF (n1 > n2)
  THEN
    SET s=n1;
  ELSE
    SET s=n2;
  END IF;
-- mostrar salida
  SELECT s;
END//
DELIMITER ;


DELIMITER //
DROP PROCEDURE IF EXISTS ej1v1c//
CREATE PROCEDURE ej1v1c (n1 int, n2 int)
  COMMENT 'Realizar un procedimiento almacenado que reciba dos números y te indique el mayor de ellos, realizarle con instrucción if
  mejoramos el algoritmo'
BEGIN
-- definir las variables
  DECLARE s int DEFAULT n2;

-- instrucciones de seleccion
  IF (n1 > n2)
  THEN
    SET s=n1;
  END IF;
-- salida
  SELECT s;
END//
DELIMITER ;

-- version 2

DELIMITER //
DROP PROCEDURE IF EXISTS ej1v2//
CREATE PROCEDURE ej1v2 (n1 int, n2 int)
  COMMENT 'Reciba dos números y te indique el mayor de ellos. Realizarle con consulta de totales'
BEGIN
  -- crear una tabla temporal
  CREATE OR REPLACE TEMPORARY TABLE ej1 (
    id int AUTO_INCREMENT,
    num int,
    PRIMARY KEY (id)
  );

  -- insertar los datos en la tabla
  INSERT INTO ej1 (num)
    VALUES (n1), (n2);

  -- calcular y mostrar el resultado
  SELECT
    MAX(num)
  FROM ej1;

END//
DELIMITER ;

-- version 3
DELIMITER //
DROP PROCEDURE IF EXISTS ej1v3//
CREATE PROCEDURE ej1v3 (IN numero1 int, IN numero2 int)
  COMMENT 'Reciba dos números y te indique el mayor de ellos. Realizarle con una función de Mysql'
BEGIN
  -- definir variable
  DECLARE r int;

  -- calcular
  SET r = GREATEST(numero1, numero2);
  
  -- mostrar
  SELECT
    CONCAT('El mayor es ', r);

END//
DELIMITER ;


/* 
  EJERCICIO 2 
  Realizar un procedimiento almacenado que reciba tres números y te indique el mayor de ellos 
  (realizarle con instrucción if, con consulta de totales y con una función de Mysql)
 */

  DELIMITER //
DROP PROCEDURE IF EXISTS ej2v1a//

  CREATE PROCEDURE ej2v1a (num1 int, num2 int, num3 int)
    COMMENT 'Reciba tres números y te indique el mayor de ellos. Realizarle con instrucción if'
  BEGIN
    IF (num1>num2) 
      THEN 
      IF (num1>num3) 
        THEN 
        SELECT num1;
      ELSE 
        SELECT num3;
      END IF;
      ELSEIF (num2>num3) 
      THEN
      SELECT num2;
    ELSE 
      SELECT num3;
    END IF;
  END //
DELIMITER;


DELIMITER //
DROP PROCEDURE IF EXISTS ej2v1b//

  CREATE PROCEDURE ej2v1b (num1 int, num2 int, num3 int)
    COMMENT 'Reciba tres números y te indique el mayor de ellos. Realizarle con instrucción if. 
    Distinto algoritmo'
  BEGIN
    DECLARE s int;
    IF (num1>num2) 
      THEN 
      IF (num1>num3) 
        THEN 
        SET s=num1;
      ELSE 
        SET s=num3;
      END IF;
      ELSEIF (num2>num3) 
      THEN
      set s=num2;
    ELSE 
      set s=num3;
    END IF;
    
    SELECT s;
  END //
DELIMITER;

DELIMITER //
DROP PROCEDURE IF EXISTS ej2v1c//

  CREATE PROCEDURE ej2v1c (num1 int, num2 int, num3 int)
    COMMENT 'Reciba tres números y te indique el mayor de ellos. Realizarle con instrucción if. 
    Distinto algoritmo'
  BEGIN
    DECLARE s int DEFAULT num3;
    IF (num1>num2) 
      THEN 
      IF (num1>num3) 
        THEN 
        SET s=num1;
      END IF;
      ELSEIF (num2>num3) 
      THEN
      set s=num2;
    END IF;
    
    SELECT s;
  END //
DELIMITER;


-- version 2

DELIMITER //
DROP PROCEDURE IF EXISTS ej2v2 //

CREATE PROCEDURE ej2v2(num1 int, num2 int, num3 int)
  COMMENT 'Reciba tres números y te indique el mayor de ellos. Realizarle con totales'
BEGIN

  DROP TABLE IF EXISTS tabla_2b;
  CREATE TEMPORARY TABLE tabla_2b(
  id int AUTO_INCREMENT PRIMARY KEY,
  numero1 int
  );
  
  INSERT INTO tabla_2b VALUES
  (DEFAULT, num1),
  (DEFAULT, num2),
  (DEFAULT, num3);

  SELECT MAX(numero1) FROM tabla_2b;

  END//
  DELIMITER ;

-- version 3

  DELIMITER //
DROP PROCEDURE IF EXISTS ej2v3//

CREATE PROCEDURE ej2v3 (num1 int, num2 int, num3 int)
  COMMENT 'Reciba tres números y te indique el mayor de ellos. Realizarle con funcion MYSQL'
BEGIN
  SELECT
    GREATEST(num1, num2, num3);
END//
DELIMITER ;

/* 
  Ejercicio 3 
  Realizar un procedimiento almacenado que reciba tres números y dos argumentos de tipo salida 
  donde devuelva el numero más grande y el numero mas pequeño de los tres números pasados
*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej3//

CREATE PROCEDURE ej3 (arg1 int, arg2 int, arg3 int, OUT mayor int, OUT menor int)
  COMMENT 'reciba tres números y dos argumentos de tipo salida donde devuelva el numero más grande 
  y el numero mas pequeño de los tres números pasados'
BEGIN
      SET mayor = GREATEST(arg1,arg2,arg3);
      SET menor = LEAST(arg1,arg2,arg3);

END//
DELIMITER ;

/* 
  Ejercicio 4 
  Realizar un procedimiento almacenado que reciba dos fechas y te muestre el número 
  de días de diferencia entre las dos fechas
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej4 $$
CREATE PROCEDURE ej4(fecha1 date, fecha2 date)
  COMMENT 'reciba dos fechas y te muestre el número de días de diferencia entre las dos fechas'
BEGIN
 
  SELECT DATEDIFF(fecha2,fecha1) dias;
 
END
$$

/* 
  Ejercicio 5 
  Realizar un procedimiento almacenado que reciba dos fechas y te muestre el número de meses de 
  diferencia entre las dos fechas
*/

DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS ej5 $$
CREATE PROCEDURE ej5(fecha1 date, fecha2 date)
  COMMENT 'reciba dos fechas y te muestre el número de meses de diferencia entre las dos fechas'
BEGIN
 
  SELECT TIMESTAMPDIFF(MONTH,fecha1,fecha2);
 
END
$$

DELIMITER ;


/* 
  ejercicio 6 
  Realizar un procedimiento almacenado que reciba dos fechas y te devuelva en 3 argumentos de salida 
  los días, meses y años entre las dos fechas
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej6 $$
CREATE PROCEDURE ej6(fecha1 date, fecha2 date, OUT dias int, OUT meses int, OUT annos int)
  COMMENT 'reciba dos fechas y te devuelva en 3 argumentos de salida los días, meses y años 
  entre las dos fechas' 
BEGIN
  SET dias=TIMESTAMPDIFF(DAY,fecha1,fecha2);
  SET meses=TIMESTAMPDIFF(MONTH,fecha1,fecha2);
  SET annos=TIMESTAMPDIFF(YEAR,fecha1,fecha2);
END
$$

DELIMITER ;

/* 
  Ejercicio 7 
  Realizar un procedimiento almacenado que reciba una frase y te muestre el número de caracteres 
*/

DELIMITER $$
DROP PROCEDURE IF EXISTS ej7 $$
CREATE PROCEDURE ej7(frase varchar(150))
  COMMENT 'Realizar un procedimiento almacenado que reciba una frase y te muestre el número de caracteres' 
BEGIN
  SELECT LENGTH(frase);
END
$$

DELIMITER ;