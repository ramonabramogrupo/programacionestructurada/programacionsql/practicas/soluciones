﻿USE practica10Disparadores;
/**
  Ejercicio 1
  Crear un disparador para la tabla ventas para que cuando metas un registro 
  nuevo te calcule el total automáticamente
**/

DELIMITER //
DROP TRIGGER IF EXISTS ventasBI //
CREATE TRIGGER ventasBI
	BEFORE INSERT
	ON ventas
	FOR EACH ROW
BEGIN
  -- cuando metas un registro 
  -- nuevo te calcule el total automáticamente
    SET NEW.total=new.unidades*new.precio;
END //

DELIMITER ;


/**
  Ejercicio 2
  Crear un disparador para la tabla ventas para que cuando inserte un registro 
  me sume el total a la tabla productos (en el campo cantidad).
**/

DELIMITER //
DROP TRIGGER IF EXISTS ventasAI //
CREATE TRIGGER ventasAI
	AFTER INSERT
	ON ventas
	FOR EACH ROW
BEGIN
  /*
    Crear un disparador para la tabla ventas para que cuando inserte un registro 
    me sume el total a la tabla productos (en el campo cantidad).
  */
    UPDATE productos p SET p.cantidad=p.cantidad+new.total 
      WHERE p.producto=new.producto;
END //
DELIMITER ;

-- comprobando
INSERT INTO ventas (producto, precio, unidades)
  VALUES ('p2', 5, 20);

SELECT * FROM ventas v;
SELECT * FROM productos p;

/**
  Ejercicio 3
  Crear un disparador para la tabla ventas para que cuando actualices un registro 
  nuevo te calcule el total automáticamente.
**/

DELIMITER //
DROP TRIGGER IF EXISTS ventasBU //
CREATE TRIGGER ventasBU
	BEFORE UPDATE
	ON ventas
	FOR EACH ROW
BEGIN
  /**
  Crear un disparador para la tabla ventas para que cuando actualices un registro 
  nuevo te calcule el total automáticamente.
  **/
    SET NEW.total=new.unidades*new.precio;
END //
DELIMITER ;


/**
  Ejercicio 4
  Crear un disparador para la tabla ventas para que cuando actualice un registro 
  me sume el total a la tabla productos (en el campo cantidad). Debe ser despues de actualizar
**/

  DELIMITER //
  DROP TRIGGER IF EXISTS ventasAU //
  CREATE TRIGGER ventasAU
  	AFTER UPDATE
  	ON ventas
  	FOR EACH ROW
  BEGIN
    /**
    Crear un disparador para la tabla ventas para que cuando actualice un registro 
    me sume el total a la tabla productos (en el campo cantidad).
    **/
      UPDATE productos p 
        SET p.cantidad=p.cantidad+(new.total-old.total) 
        WHERE p.producto=new.producto;
  END //
  DELIMITER ;

/**
  Ejercicio 5
  Crear un disparador para la tabla productos que si cambia el código del producto 
  te sume todos los totales de ese producto de la tabla ventas
**/

DELIMITER //
DROP TRIGGER IF EXISTS productosBU //
CREATE TRIGGER productosBU
	BEFORE UPDATE
	ON productos
	FOR EACH ROW
BEGIN
  /**
  Crear un disparador para la tabla productos que si cambia el código del producto 
  te sume todos los totales de ese producto de la tabla ventas
  **/
    SET NEW.cantidad=(
          SELECT sum(v.total) FROM ventas v 
            WHERE v.producto=NEW.producto
          );
END //
DELIMITER ;

/**
  Ejercicio 6
  Crear un disparador para la tabla productos que si eliminas 
  un producto te elimine todos los productos del mismo código 
  en la tabla ventas

**/

DELIMITER //
DROP TRIGGER IF EXISTS productosAD //
CREATE TRIGGER productosAD
	AFTER DELETE
	ON productos
	FOR EACH ROW
BEGIN
 
/**
  Crear un disparador para la tabla productos que si eliminas 
  un producto te elimine todos los productos del mismo código 
  en la tabla ventas
**/
    DELETE FROM ventas 
      WHERE producto=OLD.producto;
END //
DELIMITER ;

/**
  Ejercicio 7
  Crear un disparador para la tabla ventas que si eliminas un 
  registro te reste el total del campo cantidad de la tabla 
  productos (en el campo cantidad).
**/

DELIMITER //
DROP TRIGGER IF EXISTS ventasAD //
CREATE TRIGGER ventasAD
	AFTER DELETE
	ON ventas
	FOR EACH ROW
BEGIN
/**
  Crear un disparador para la tabla ventas que si eliminas un 
  registro te reste el total del campo cantidad de la tabla 
  productos (en el campo cantidad).
**/
    UPDATE productos 
      SET cantidad=cantidad-old.total 
      WHERE producto=old.producto;
END //

DELIMITER ;

