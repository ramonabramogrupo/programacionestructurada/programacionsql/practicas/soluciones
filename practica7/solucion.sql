﻿DROP DATABASE IF EXISTS practica6programacion;
CREATE DATABASE practica6programacion;
USE practica6programacion;

DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio1 $$
CREATE PROCEDURE ejercicio1()
BEGIN
  SELECT "¡Hola clase de Alpe!";
END
$$

DELIMITER ;

CALL ejercicio1();


DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio2 $$
CREATE PROCEDURE ejercicio2(IN numero int)
BEGIN
  IF (numero>=0) THEN
    SELECT "positivo";
  ELSE
    SELECT "negativo";
  END IF;
END
$$

DELIMITER ;

CALL ejercicio2(-5);


DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio3 $$
CREATE PROCEDURE ejercicio3(IN numero int)
BEGIN
  IF (numero=0) THEN
    SELECT "cero";
  ELSEIF(numero>0) THEN 
    SELECT "positivo";
  ELSE
    SELECT "negativo";
  END IF;
END
$$

DELIMITER ;

CALL ejercicio3(1);


DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio4 $$
CREATE PROCEDURE ejercicio4(IN numero int)
BEGIN
CASE 
  WHEN (numero=0)  THEN 
    SELECT "cero";
  WHEN (numero>0)  THEN 
    SELECT "positivo";
  ELSE 
    SELECT "negativo";
END CASE;

END
$$

DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio5 $$
CREATE PROCEDURE ejercicio5()
BEGIN
/* variables */
DECLARE salida varchar(100);
/* programa */
IF (numero=0) THEN
    SET salida="cero";
  ELSEIF(numero>0) THEN 
    SET salida="positivo";
  ELSE
    set salida="negativo";
  END IF;
  SELECT salida;
END
$$

DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS ejercicio6 $$
CREATE PROCEDURE ejercicio4(IN numero int)
BEGIN
DECLARE salida varchar(100);

CASE 
  WHEN (numero=0)  THEN 
    set salida="cero";
  WHEN (numero>0)  THEN 
    set salida="positivo";
  ELSE 
    SET salida="negativo";
END CASE;
  SELECT salida;
END
$$

DELIMITER ;