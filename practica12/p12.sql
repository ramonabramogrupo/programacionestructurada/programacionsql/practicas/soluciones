﻿DROP DATABASE IF EXISTS practica12Disparadores;
CREATE DATABASE practica12Disparadores;
USE practica12Disparadores;

-- Ejercicio 1

CREATE TABLE alumnos(
  id int UNSIGNED,
  nombre varchar(50),
  apellido1 varchar(150),
  apellido2 varchar(150),
  nota float,
  PRIMARY KEY(id)
  );


-- Ejercicio 2

  DROP TRIGGER IF EXISTS alumnosBI;
  CREATE TRIGGER alumnosBI
  BEFORE INSERT
    ON alumnos
    FOR EACH ROW
  BEGIN
    IF (NEW.nota<0) THEN 
      SET NEW.nota=0;
    ELSEIF (NEW.nota>100) THEN 
      SET NEW.nota=100;
    ELSE
      SET NEW.nota=ROUND(NEW.nota,1);
    END IF;
    
  END;

-- Ejercicio 3
  
  DROP TRIGGER IF EXISTS alumnosBU;
  CREATE TRIGGER alumnosBU
  BEFORE UPDATE
    ON alumnos
    FOR EACH ROW
  BEGIN
    IF (NEW.nota<0) THEN 
      SET NEW.nota=0;
    ELSEIF (NEW.nota>100) THEN 
      SET NEW.nota=100;
    ELSE
      SET NEW.nota=ROUND(NEW.nota,1);
    END IF;
    
  END;

-- Ejercicio 4

  DROP TRIGGER IF EXISTS alumnosBU1;
  CREATE TRIGGER alumnosBU1
  BEFORE UPDATE
    ON alumnos
    FOR EACH ROW
  BEGIN
    IF (NEW.nota<OLD.nota) THEN 
      SET NEW.nota=OLD.nota;
    END IF;
  END;

-- Ejercicio 5

  CREATE TABLE notas(
    id int,
    numero int COMMENT 'numero de alumnos con esa nota',
    PRIMARY KEY(id)
    );

  INSERT INTO notas (id, numero)
  VALUES (0, 0),(10,0),(20,0),(30,0),(40,0),(50,0),(60,0),(70,0),(80,0),(90,0),(100,0);


-- ejercicio 6

DROP TRIGGER IF EXISTS alumnosAI;
  CREATE TRIGGER alumnosAI
  AFTER INSERT
    ON alumnos
    FOR EACH ROW
  BEGIN
    UPDATE notas n 
      SET n.numero=n.numero+1
      WHERE n.id=TRUNCATE(NEW.nota,-1);
  END;

-- ejercicio 7

DROP TRIGGER IF EXISTS alumnosAU;
  CREATE TRIGGER alumnosAU
  AFTER UPDATE
    ON alumnos
    FOR EACH ROW
  BEGIN
    UPDATE notas n
      SET n.numero=n.numero-1
      WHERE n.id=TRUNCATE(OLD.nota,-1);

    UPDATE notas n 
      SET n.numero=n.numero+1
      WHERE n.id=TRUNCATE(NEW.nota,-1);
  END;

-- ejercicio 8

DROP TRIGGER IF EXISTS alumnosAD;
  CREATE TRIGGER alumnosAD
  AFTER DELETE
    ON alumnos
    FOR EACH ROW
  BEGIN
    UPDATE notas n
      SET n.numero=n.numero-1
      WHERE n.id=TRUNCATE(OLD.nota,-1);
  END;







