﻿DROP DATABASE IF EXISTS procedimientos1;
CREATE DATABASE procedimientos1;

USE procedimientos1;

-- crear procedimiento almacenado
DROP PROCEDURE IF EXISTS uno;
CREATE PROCEDURE uno()
  COMMENT 'ejemplo de procedimiento almacenado'
  BEGIN 
    -- elimino la tabla si existe
    DROP TABLE IF EXISTS ciudades;
  /* Creo la tabla */
    CREATE TABLE ciudades(
      id int AUTO_INCREMENT,
      nombre varchar(100),
      PRIMARY KEY(id)
    );
  END;

  -- mostrar el codigo del procediento
  SHOW CREATE PROCEDURE uno;

  -- mostrar procedimientos de la base de datos  
  SHOW PROCEDURE STATUS WHERE db="procedimientos1";
  
  -- otra opcion
  SELECT * FROM mysql.proc p WHERE p.type="procedure" AND p.db="procedimientos1";

  -- ejecutamos el procedimiento
  CALL uno();

  -- comprobar que existe la tabla
    SHOW TABLES FROM procedimientos1;
 
  -- compronar estructura de la tabla
    DESCRIBE ciudades;
    EXPLAIN ciudades;


-- crear procedimiento almacenado dos
DROP PROCEDURE IF EXISTS dos;
CREATE PROCEDURE dos()
  COMMENT 'procedimiento que me crear la tabla personas'
  BEGIN 
    -- elimino la tabla si existe
    DROP TABLE IF EXISTS personas;
  /* Creo la tabla */
    CREATE TABLE personas(
      id int AUTO_INCREMENT,
      nombre varchar(100),
      edad int,
      PRIMARY KEY(id)
    );
  END;  

 -- llamando al procedimiento
 CALL dos();


  -- crear la funcion f1
  DROP FUNCTION IF EXISTS f1;
  CREATE FUNCTION f1()
    RETURNS varchar(50)
    BEGIN
      RETURN CONCAT_WS(" ","hoy es",CURDATE(),"y son",CURTIME());
    END;

-- llamar a la funcion
 SELECT f1();

-- comprobar funciones
SHOW FUNCTION STATUS WHERE db="procedimientos1";

-- codigo de la funcion
SHOW CREATE FUNCTION f1;

