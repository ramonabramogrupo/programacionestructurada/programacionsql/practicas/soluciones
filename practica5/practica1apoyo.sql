﻿DROP DATABASE IF EXISTS practica5m1u3;
CREATE DATABASE IF NOT EXISTS practica5m1u3
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

USE practica5m1u3;


DELIMITER $$

--
-- Definition for procedure ejercicio1
--
CREATE PROCEDURE ejercicio1 (IN lado float)
BEGIN
  -- declaramos las variables
  DECLARE area float;
  DECLARE superficie float;

  -- almacenamos en las variables las areas y las superficies
  SET area = POW(lado, 2);
  SET superficie = lado * 4;

  -- creamos la tabla si no existe con los resultados
  CREATE TABLE IF NOT EXISTS ejercicio1 (
    numero int AUTO_INCREMENT,
    lado float,
    area float,
    supertificie float,
    PRIMARY KEY (numero)
  );

  -- almacenamos los resultados
  INSERT INTO ejercicio1
    VALUES (DEFAULT, lado, area, superficie);

END
$$

--
-- Definition for procedure ejercicio10
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio10 (IN lado float)
BEGIN
/*
  -- declaramos las variables
  DECLARE volumen float;
  DECLARE areaTotal float;
  DECLARE areaLateral float;


  -- almacenamos en las variables los resultados de calcular el volumen, el area Total y el area Lateral
  



  -- creamos la tabla si no existe con los resultados
  CREATE TABLE IF NOT EXISTS ejercicio10 (
    id int AUTO_INCREMENT,
    
    PRIMARY KEY (id)
  );

  -- almacenamos los resultados
  INSERT INTO ejercicio10
    VALUES (DEFAULT, lado, volumen, areaTotal, areaLateral);

*/
END
$$

--
-- Definition for procedure ejercicio10S
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio10S (IN radio float, IN altura float)
BEGIN

  -- declaramos las variables
  DECLARE volumen float;
  DECLARE areaTotal float;
  DECLARE areaLateral float;


  -- almacenamos en las variables los resultados de calcular el volumen, el area Total y el area Lateral
  SET volumen = ejercicio7(radio, alura);
  SET areaLateral = ejercicio8(radio, altura);
  SET areaTotal = ejercicio9(radio, altura);

  -- creamos la tabla si no existe con los resultados
  CREATE TABLE IF NOT EXISTS ejercicio10 (
    id int AUTO_INCREMENT,
    radio float,
    altura float,
    areaLateralCampo float,
    areaTotalCampo float,
    volumenCampo float,
    PRIMARY KEY (id)
  );

  -- almacenamos los resultados
  INSERT INTO ejercicio10
    VALUES (DEFAULT, radio, altura, areaLateral, areaTotal, volumen);


END
$$

--
-- Definition for procedure ejercicio2
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio2 (IN lado float)
BEGIN
/*
  -- declaramos las variables
   

  -- almacenamos en las variables el volumen
  set volumen = POW(lado,3);
  

  -- creamos la tabla si no existe con los resultados
  CREATE TABLE if NOT EXISTS ejercicio2
    (
      id int AUTO_INCREMENT,
      ladoCampo float,
      volumenCampo float,
      PRIMARY KEY (id)
    );

  -- almacenamos los resultados
  INSERT INTO ejercicio2 VALUES();
*/
END
$$

--
-- Definition for procedure ejercicio2S
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio2S (IN lado float)
BEGIN

  -- declaramos las variables
  DECLARE volumen float;

  -- almacenamos en las variables el volumen
  SET volumen = POW(lado, 3);


  -- creamos la tabla si no existe con los resultados
  CREATE TABLE IF NOT EXISTS ejercicio2 (
    id int AUTO_INCREMENT,
    ladoCampo float,
    volumenCampo float,
    PRIMARY KEY (id)
  );

  -- almacenamos los resultados
  INSERT INTO ejercicio2
    VALUES (DEFAULT, lado, volumen);


END
$$

--
-- Definition for procedure ejercicio3
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio3 (IN lado float)
BEGIN
/*  
  -- declaramos las variables
  

  -- almacenamos en las variables el volumen y el area total
  -- el volumen es ladoxladoxlado
  -- el area total es 6xladoxlado
  
  

  -- creamos la tabla si no existe con los resultados
  CREATE TABLE if NOT EXISTS ejercicio3
    (
      id int AUTO_INCREMENT,
      ladoCampo float,
      volumenCampo float,
      areaTotalCampo float,
      PRIMARY KEY (id)
    );

  -- almacenamos los resultados
  

*/
END
$$

--
-- Definition for procedure ejercicio3S
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio3S (IN lado float)
BEGIN

  -- declaramos las variables
  DECLARE volumen float;
  DECLARE areaTotal float;


  -- almacenamos en las variables el volumen y el area total
  -- el volumen es ladoxladoxlado
  -- el area total es 6xladoxlado
  SET areaTotal = 6 * POW(lado, 2);
  SET volumen = POW(lado, 3);


  -- creamos la tabla si no existe con los resultados
  CREATE TABLE IF NOT EXISTS ejercicio3 (
    id int AUTO_INCREMENT,
    ladoCampo float,
    volumenCampo float,
    areaTotalCampo float,
    PRIMARY KEY (id)
  );

  -- almacenamos los resultados
  INSERT INTO ejercicio3
    VALUES (DEFAULT, lado, volumen, areaTotal);


END
$$

--
-- Definition for procedure ejercicio6
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio6 (IN lado float)
BEGIN

  -- declaramos las variables
  DECLARE volumen float;
  DECLARE areaTotal float;


  -- almacenamos en las variables los resultados de calcular el volumen y el area Total
  SET volumen = ejercicio1(lado);



  -- creamos la tabla si no existe con los resultados
  CREATE TABLE IF NOT EXISTS ejercicio6 (
    id int AUTO_INCREMENT,
    ladoCampo float,
    volumenCampo float,
    PRIMARY KEY (id)
  );

  -- almacenamos los resultados
  INSERT INTO ejercicio6
    VALUES (DEFAULT, lado, volumen);


END
$$

--
-- Definition for procedure ejercicio6S
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE ejercicio6S (IN lado float)
BEGIN

  -- declaramos las variables
  DECLARE volumen float;
  DECLARE areaTotal float;


  -- almacenamos en las variables los resultados de calcular el volumen y el area Total
  SET volumen = ejercicio1(lado);
  SET areaTotal = ejercicio2S(lado);



  -- creamos la tabla si no existe con los resultados
  CREATE TABLE IF NOT EXISTS ejercicio6 (
    id int AUTO_INCREMENT,
    ladoCampo float,
    volumenCampo float,
    areaTotalCampo float,
    PRIMARY KEY (id)
  );

  -- almacenamos los resultados
  INSERT INTO ejercicio6
    VALUES (DEFAULT, lado, volumen, areaTotal);


END
$$

--
-- Definition for function ejercicio1
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio1 (lado float)
RETURNS float
BEGIN
  DECLARE volumen float;

  SET volumen = POW(lado, 3);

  RETURN volumen;
END
$$

--
-- Definition for function ejercicio2
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio2 (lado int)
RETURNS float
BEGIN
  DECLARE areaTotal float;



  RETURN areaTotal;
END
$$

--
-- Definition for function ejercicio2S
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio2S (lado int)
RETURNS float
BEGIN
  DECLARE areaTotal float;

  SET areaTotal = 6 * POW(lado, 2);

  RETURN areaTotal;
END
$$

--
-- Definition for function ejercicio7
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio7 (radio float, altura float)
RETURNS float
BEGIN
  DECLARE volumen float;

  SET volumen = PI() * POW(radio, 2) * altura;

  RETURN volumen;
END
$$

--
-- Definition for function ejercicio8
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio8 (radio float, altura float)
RETURNS float
BEGIN
  DECLARE areaLateral float;

  SET areaLateral = 2 * PI() * radio * altura;

  RETURN areaLateral;
END
$$

--
-- Definition for function ejercicio9
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION ejercicio9 (radio float, altura float)
RETURNS float
BEGIN
  DECLARE areaTotal float;

  SET areaTotal = 2 * PI() * radio * (altura + radio);

  RETURN areaLateral;
END
$$

DELIMITER ;



--
-- Definition for table ejercicio1
--
CREATE TABLE IF NOT EXISTS ejercicio1 (
  numero int(11) NOT NULL AUTO_INCREMENT,
  lado float DEFAULT NULL,
  area float DEFAULT NULL,
  supertificie float DEFAULT NULL,
  PRIMARY KEY (numero)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

--
-- Definition for table ejercicio2
--
CREATE TABLE IF NOT EXISTS ejercicio2 (
  id int(11) NOT NULL AUTO_INCREMENT,
  ladoCampo float DEFAULT NULL,
  volumenCampo float DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

--
-- Definition for table ejercicio3
--
CREATE TABLE IF NOT EXISTS ejercicio3 (
  id int(11) NOT NULL AUTO_INCREMENT,
  ladoCampo float DEFAULT NULL,
  volumenCampo float DEFAULT NULL,
  areaTotalCampo float DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

--
-- Definition for table ejercicio6
--
CREATE TABLE IF NOT EXISTS ejercicio6 (
  id int(11) NOT NULL AUTO_INCREMENT,
  ladoCampo float DEFAULT NULL,
  volumenCampo float DEFAULT NULL,
  areaTotalCampo float DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 2
AVG_ROW_LENGTH = 16384
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

-- 
-- Dumping data for table ejercicio1
--
INSERT INTO ejercicio1 VALUES
(1, 2, 4, 8);

-- 
-- Dumping data for table ejercicio2
--
INSERT INTO ejercicio2 VALUES
(1, 2, 8);

-- 
-- Dumping data for table ejercicio3
--
INSERT INTO ejercicio3 VALUES
(1, 2, 8, 24);

-- 
-- Dumping data for table ejercicio6
--
INSERT INTO ejercicio6 VALUES
(1, 2, 8, 24);

